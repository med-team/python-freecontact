#!/usr/bin/python
#   FreeContact - program to predict protein residue contacts from a sufficiently large multiple alignment
#   Copyright (C) 2013 by Laszlo Kajan, Technical University of Munich, Germany
# 
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
# 
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
# 
#   You should have received a copy of the GNU General Public License
#   along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
from distutils.core import setup, Extension
from glob import glob

setup(	name = 'python-freecontact',
    	version = '1.1',
    	author = 'Laszlo Kajan',
    	author_email = 'lkajan@rostlab.org',
    	description = 'fast protein contact predictor',
        #long_description = '',
        #download_url = '',
        license = 'GPL-3+',
    	url = 'http://rostlab.org/',
        ext_modules = [Extension(
            name = 'freecontact',
            sources = glob('src/*.cpp'),
            libraries = ['freecontact', 'boost_python'])]
	)

# vim:et:ts=4:ai:
